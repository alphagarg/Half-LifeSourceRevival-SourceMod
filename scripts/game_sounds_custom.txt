"HudChat.Message"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"		"95"
	"soundlevel"	"0"

	"wave"			"common/menu2.wav"
}

"Bullets.Crack"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"87,100"
	"rndwave"
	{
		wave	"^physics/bullets/crack.wav"
		wave	"^physics/bullets/crack.wav"
		wave	"^physics/bullets/crack.wav"
		wave	"^weapons/rics/ric1.wav"
		wave	"^weapons/rics/ric2.wav"
		wave	"^weapons/rics/ric3.wav"
		wave	"^weapons/rics/ric4.wav"
		wave	"^weapons/rics/ric5.wav"
		wave	"^physics/bullets/snap01.wav"
		wave	"^physics/bullets/snap02.wav"
		wave	"^physics/bullets/snap03.wav"
		wave	"^physics/bullets/snap04.wav"
		wave	"^physics/bullets/snap05.wav"
		wave	"^physics/bullets/snap06.wav"
		wave	"^physics/bullets/snap07.wav"
		wave	"^physics/bullets/snap08.wav"
		wave	"^physics/bullets/snap09.wav"
	}
}

"Bullets.HeavyCrack"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"^physics/bullets/heavy_crack.wav"
}

"HL3_Music.track_1"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#music/so_i_arrived...wav"
}

"Bullets.VehicleCookOff"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"^physics/bullets/cookoff.wav"
}

// M249

"Weapon_M249.Single"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"^weapons/m249/m249.wav"
}

"Weapon_M249.OpenLid"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"^weapons/m249/m249_coverup.wav"
}

"Weapon_M249.BoxOut"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"^weapons/m249/m249_boxout.wav"
}

"Weapon_M249.BoxIn"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"^weapons/m249/m249_boxin.wav"
}

"Weapon_M249.BulletsLoad"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"^weapons/m249/m249_chain.wav"
}

"Weapon_M249.CloseLid"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"^weapons/m249/m249_coverdown.wav"
}

// Dual Deags

"Weapon_Deag.Single"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"^weapons/deagle/deagle_fire.wav"
}

"Weapon_Deag.Special1"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"weapons/deagle/deagle_clipout.wav"
}

"Weapon_Deag.Special2"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"weapons/deagle/deagle_clipin.wav"
}

"Weapon_Deag.Slide"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"weapons/deagle/deagle_slide.wav"
}

"Weapon_Deag.Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"weapons/deagle/deagle_draw.wav"
}

"Weapon_Deag.Holster"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"weapons/deagle/deagle_holster.wav"
}

// M40A1

"Weapon_M40A1.Single"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"		"PITCH_NORM"
	"wave"	"^weapons/m40a1/m40a1.wav"
}

// Auto Shotgun

"Weapon_ShotgunHD.Single"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"soundlevel"		"SNDLVL_GUNFIRE"

	"wave"			"^weapons/shotgunhd/shoot.wav"
}