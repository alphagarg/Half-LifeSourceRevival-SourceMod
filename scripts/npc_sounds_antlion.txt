//Panthereye, actually

"NPC_Antlion.MeleeAttack"
{
	"channel"		"CHAN_ITEM"
	"volume"		"VOL_NORM"
	"pitch"			"90, 100"

	"soundlevel"	"SNDLVL_NORM"

	"rndwave"
	{
		"wave"	"npc/zombie/claw_strike1.wav"
		"wave"	"npc/zombie/claw_strike2.wav"
		"wave"	"npc/zombie/claw_strike3.wav"
	}
}

"NPC_Antlion.FootstepSoft"
{
	"channel"		"CHAN_BODY"
	"volume"		"0.030, 0.050"
	"pitch"			"105, 115"

	"soundlevel"	"SNDLVL_NORM"

	"rndwave"
	{
		"wave"	"npc/vort/vort_foot1.wav"
		"wave"	"npc/vort/vort_foot2.wav"
		"wave"	"npc/vort/vort_foot3.wav"
		"wave"	"npc/vort/vort_foot4.wav"
	}
}

"NPC_Antlion.Footstep"
{
	"channel"		"CHAN_BODY"
	"volume"		"0.100, 0.150"
	"pitch"			"100, 110"

	"soundlevel"	"SNDLVL_NORM"

	"rndwave"
	{
		"wave"	"npc/vort/vort_foot1.wav"
		"wave"	"npc/vort/vort_foot2.wav"
		"wave"	"npc/vort/vort_foot3.wav"
		"wave"	"npc/vort/vort_foot4.wav"
	}
}

"NPC_Antlion.FootstepHeavy"
{
	"channel"		"CHAN_BODY"
	"volume"		"VOL_NORM"
	"pitch"			"PITCH_NORM"

	"soundlevel"	"SNDLVL_NORM"

	"rndwave"
	{
		"wave"	"npc/vort/vort_foot1.wav"
		"wave"	"npc/vort/vort_foot2.wav"
		"wave"	"npc/vort/vort_foot3.wav"
		"wave"	"npc/vort/vort_foot4.wav"
	}
}

"NPC_Antlion.MeleeAttackSingle"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"0.250, 1.000"
	"pitch"			"85, 95"

	"soundlevel"	"SNDLVL_NORM"

	"rndwave"
	{
		"wave"		"zombie/claw_strike1.wav"
		"wave"		"zombie/claw_strike2.wav"
		"wave"		"zombie/claw_strike3.wav"
	}
}

"NPC_Antlion.MeleeAttackDouble"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"0.250, 1.000"
	"pitch"			"85, 95"

	"soundlevel"	"SNDLVL_NORM"

	"rndwave"
	{
		"wave"		"zombie/claw_strike1.wav"
		"wave"		"zombie/claw_strike2.wav"
		"wave"		"zombie/claw_strike3.wav"
	}
}

"NPC_Antlion.Idle"
{
	"channel"		"CHAN_VOICE"
	"volume"		"0.750, 1.000"
	"pitch"			"95, 105"

	"soundlevel"	"SNDLVL_NORM"

	"rndwave"
	{
		"wave"		"garg/gar_attack1.wav"
		"wave"		"garg/gar_attack2.wav"
		"wave"		"garg/gar_attack3.wav"
	}
}

"NPC_Antlion.Pain"
{
	"channel"		"CHAN_VOICE"
	"volume"		"0.750, 1.000"
	"pitch"			"95, 105"

	"soundlevel"	"SNDLVL_NORM"

	"rndwave"
	{
		"wave"		"garg/gar_pain1.wav"
		"wave"		"garg/gar_pain2.wav"
		"wave"		"garg/gar_pain3.wav"
	}
}

"NPC_Antlion.JumpTouch"
{
	"channel"		"CHAN_ITEM"
	"volume"		"VOL_NORM"
	"pitch"			"PITCH_NORM"

	"soundlevel"	"SNDLVL_IDLE"

	"rndwave"
	{
		"wave"	"npc/zombie/claw_strike1.wav"
		"wave"	"npc/zombie/claw_strike2.wav"
		"wave"	"npc/zombie/claw_strike3.wav"
	}
}

"NPC_Antlion.Land"
{
	"channel"		"CHAN_BODY"
	"volume"		"VOL_NORM"
	"pitch"			"95, 105"

	"soundlevel"	"SNDLVL_NORM"

	"wave"			"npc/vort/foot_hit.wav"
}

"NPC_Antlion.WingsOpen"
{
	"channel"		"CHAN_STATIC"
	"volume"		"VOL_NORM"
	"pitch"			"PITCH_NORM"

	"soundlevel"	"SNDLVL_NORM"

	"wave"			"vehicles/fast_windloop1.wav"
}

"NPC_Antlion.RunOverByVehicle"
{
	"channel"	"CHAN_VOICE"
	"volume"	"VOL_NORM"
	"soundlevel"  "SNDLVL_NORM"
	"pitch"	"PITCH_NORM"

	"wave"		"common/bodysplat.wav"
}